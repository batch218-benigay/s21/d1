console.log("S21 Discusion");
console.log("--------------------------------");

//List of StudentIDs of all graduating sudent of the class.
console.log("Variable VS Array");

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);

						//0				//1			//2			//3			//4
let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];
//inside the [] are called elements

console.log(studentNumbers);

console.log("--------------------------------");
/* [SECTION] ARRAYS
    - Arrays are used to store multiple related values in a single variable.
    - They are declared using square brackets ([]) also known as "Array Literals"
    - Arrays it also provides access to a number of functions/methods that help in manipulation array.
    - Methods are used to manipulate information stored within the same object.
    - Array are also objects which is another data type.
    - The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
        - Syntax:
            let/const arrayName = [elementA, elementB, elementC, ... , ElementNth];
    */

   console.log("Examples of Array: ");
   //common examples of arrays

   		let grades = [98.5, 94.3, 89.2, 90]; //arrays could store numeric values
   		let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"]; // arrays can store string value

   		console.log(grades);
   		console.log(computerBrands);

   		let mixedArray = ["John", "Doe", 12, false, null, undefined, {}]; // possible but not recommened because it's hard to identify the purpose of the  array or what are the data inside the array and where the programmer or co-programmer could use it.
   		console.log(mixedArray);

   		//ALTERNATIVE WAY OF CREATING AN ARRAY
   		let myTasks = [
   			"drink html",
   			"eat javascript",
   			"inhale css",
   			"bake mongoDb"
   		];
   		console.log(myTasks);

console.log("--------------------------------");

//Creating an array with values from variables
	
	let city1 = "Tokyo";
	let city2 = "Manila";
	let city3 = "Jakarta";

	let cities = [city1, city2, city3];
	console.log(cities);

console.log("--------------------------------");

/*[SECTION].length property
	-allows us to gt and set the total number of items 	in an array. */

	console.log("Using the .length property for array size");
	console.log("Using / size of myTasks array: " +myTasks.length); // output is 4
	console.log("The length of the cities array: " +cities.length); // output is 3

console.log("--------------------------------");

	console.log("Using .length property for string size");

		let fullName = "Alissa Mae S. Benigay";
		console.log("Length/size of fullName string variable: " +fullName.length);

console.log("--------------------------------");
	/*
	myTasks = [
	   			"drink html",
	   			"eat javascript",
	   			"inhale css",
	   			"bake mongoDb"
	   		];*/
	console.log("Removing the last element from an array");
	myTasks.length = myTasks.length - 1;
		//94				//4
	console.log(myTasks.length);
	console.log(myTasks);

	// To delete a specific item in an array, we can employ array methods. We have shown the logic or algorithm or "pop method".

	// cities = [city1, city2, city3];
	cities.length--; //output should be 2
	console.log(cities);

	// fullName = "Alissa Mae S. Benigay";
	fullName.length = fullName.length - 1;
	console.log(fullName.length);
	console.log(fullName);
	//Deleting is not possible on string

console.log("--------------------------------");
	//We can also add the length of an array.

	console.log("Add an element to an array");

	let theBeatles = [
			"John", //0
			"Paul",  //1
			"Ringo", //2
			"George"  //3
	];
	console.log(theBeatles);

	// theBeatles[4] = "Cardo";
	theBeatles[theBeatles.length] = "Cardo";
	console.log(theBeatles);

console.log("--------------------------------");
/* [SECTION] Reading from Arrays
		- Accessing array elements is one of the common task that we do with an array.
	    - This can be done through the use of array indexes.
	    - Each element in an array is associated with it's own index number.
	    - The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
	    - Array indexes it is actually refer to a memory address/location

	        Array Address: 0x7ffe942bad0
	        Array[0] = 0x7ffe942bad0
	        Array[1] = 0x7ffe942bad4
	        Array[2] = 0x7ffe942bad8

	        - Syntax
	            arrayName[index];
	    */
	    //grades = [98.5, 94.3, 89.2, 90];
	    console.log(grades[0]); // The index number of the first element is zero

	    //computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"]; 
	    console.log(computerBrands[3]);

	    //Accessing an array element that doesn't exist - will return undefined

	    console.log(grades[20]); // output is undefined

	    let lakersLegends = [
	    	"Kobe",
	    	"Shaq",
	    	"LeBron",
	    	"Magic",
	    	"Kareem",
	    	"Westbrook"
	    ];
	    //try to access and display shaq and magic

	   console.log(lakersLegends[1]);
	   console.log(lakersLegends[3]);

	   /*or
	   console.log(lakersLegends[1] +" & " + lakersLegends[3]); */

console.log("--------------------------------");
console.log("Reassigning an element from an array");
	/*You can also reassign array values using indeces*/

	console.log("Array before reassignment: ");
	console.log(lakersLegends);

	lakersLegends[2] = "Gasol";
	console.log("Array after reassignment: ");
	console.log(lakersLegends);

console.log("--------------------------------");
console.log("Access the last element of an array");

	let bullsLegends = [
			"Jordan",
			"Pipper",
			"Rodman",
			"Rose",
			"Kukoc"
	];

	let lastElementIndex = bullsLegends.length-1;
	console.log(bullsLegends[lastElementIndex]);

console.log("--------------------------------");
	console.log("Adding new items into an array using indeces");

	//Adding items into an array by using indeces

	const newArr = [];
	console.log(newArr[0]); // will return undefined - no content yet

	newArr[0] = "Cloud Strife";
	console.log(newArr);

	console.log(newArr[1]); //undefined
	newArr[1] = "Tifa Lockhart";
	console.log(newArr);

console.log("--------------------------------");
console.log("Add element in the end of an array using the '.length' property");
	
	// No -1 after .length since we're adding another element
	newArr[newArr.length] = "Barret Wallace";
	console.log(newArr);

console.log("--------------------------------");
console.log("Displaying the content of an array 1 by 1");
	//['Cloud Strife', 'Tifa Lockhart', 'Barret Wallace']

	/*EXAMPLE OF ACCESSING ELEMENTS 1 BY 1 WITH console.log conly
		console.log(newArr[0]);
		console.log(newArr[1]);
		console.log(newArr[2]);*/

	/*initialization/declaration (let i=0);
	condition (i < newArr.length);
	change of value / iteration (i++) */

	/* Looping over an array
		You can use a for loop to iterate over all items in an array */
	for(let index=0; index < newArr.length; index++){
		// To be able to show each array items in the console.log
		console.log(newArr[index]);
	}

console.log("--------------------------------");
console.log("Filtering an array using loop and conditional statements: ");

	let numArr = [5, 12, 30, 46, 40, 52];

		//This code is to check which number is divisible by 5
		for (let i=0; i < numArr.length; i++){
			if (numArr[i] % 5 == 0){
			console.log(numArr[i] + " is divisible by 5");
			}
			else{
				console.log(numArr[i] + " is not divisible by 5.");
			}
		}

console.log("--------------------------------");

/*[SECTION] MULTIDIMENSIONAL ARRAYS
	-Multidimensional arrays are useful for storing complex data structures.
	- A practical application of this is to help visualize/create real world objects.
	- This is frequently used to store data for mathematic computations, image processing, and record management.
	- Array within an Array */

	//Create a Chessboard

	let chessBoard = [
	    ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	    ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	    ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	    ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	    ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	    ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	    ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	    ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
	];
	console.table(chessBoard);

/* Access an element of a multidimensional arrays
	Syntax: multiArra[outerArr][innerArr]
						col       row */

	console.log(chessBoard[3][4]); // e4
	console.log("Pawn moves to: " +chessBoard[2][5]);

// arr =[] // deletes all the elements in an array